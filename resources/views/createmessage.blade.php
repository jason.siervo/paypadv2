@extends('voyager::master')

@section('content')
    <div class="row">
        <div class="col-md-12">
                <form action="addmessage" method="post">
                    {{ csrf_field() }}
                    <input type="hidden" name="from" value="{{ auth()->user()->email }}">
                    <label for="">Select Recipient</label>
                    <select name="to" class="form-control">
                        @foreach ($users as $user)
                            <option value="{{ $user->id}}">{{ $user->email }}</option>
                        @endforeach
                    </select>
                    <label for="">Message</label>
                    <input class="form-control" type="text" name="message">
                    <input class="pull-right btn btn-success" type="Submit"></input>
                </form>
                
        </div>
    </div>
@endsection