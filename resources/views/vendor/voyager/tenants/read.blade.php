<style>
    body {font-family: Arial;}
    
    /* Style the tab */
    .tab {
        overflow: hidden;
        border: 1px solid #FFFFFF;
        background-color: #FFFFFF;
    }
    
    /* Style the buttons inside the tab */
    .tab button {
        background-color: inherit;
        float: left;
        border: none;
        outline: none;
        cursor: pointer;
        padding: 14px 16px;
        transition: 0.3s;
        font-size: 17px;
    }
    
    /* Change background color of buttons on hover */
    .tab button:hover {
        background-color: #ddd;
    }
    
    /* Create an active/current tablink class */
    .tab button.active {
        background-color: #ccc;
    }
    
    /* Style the tab content */
    .tabcontent {
        display: none;
        padding: 6px 12px;
        background-color: #FFFFFF;
        border: 1px solid #FFFFFF;
        border-top: none;
    }
</style>

@extends('voyager::master')

@section('page_title', __('voyager::generic.view').' '.$dataType->display_name_singular)

@section('page_header')
    <h1 class="page-title">
        <i class="{{ $dataType->icon }}"></i> {{ __('voyager::generic.viewing') }} {{ ucfirst($dataType->display_name_singular) }} &nbsp;

        @can('edit', $dataTypeContent)
        <a href="{{ route('voyager.'.$dataType->slug.'.edit', $dataTypeContent->getKey()) }}" class="btn btn-info">
            <span class="glyphicon glyphicon-pencil"></span>&nbsp;
            {{ __('voyager::generic.edit') }}
        </a>
        @endcan
        @can('delete', $dataTypeContent)
            <a href="javascript:;" title="{{ __('voyager::generic.delete') }}" class="btn btn-danger delete" data-id="{{ $dataTypeContent->getKey() }}" id="delete-{{ $dataTypeContent->getKey() }}">
                <i class="voyager-trash"></i> <span class="hidden-xs hidden-sm">{{ __('voyager::generic.delete') }}</span>
            </a>
        @endcan

        <a href="{{ route('voyager.'.$dataType->slug.'.index') }}" class="btn btn-warning">
            <span class="glyphicon glyphicon-list"></span>&nbsp;
            {{ __('voyager::generic.return_to_list') }}
        </a>
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')
    <div class="page-content read container-fluid">
        <div class="row">
            <div class="col-md-12">

                @foreach($dataType->readRows as $row)

                @if($row->display_name == "Last Name")
                    <div class="col-md-12">
                        <br><h3 style="margin-left:-13px;">Personal Information</h3><br>
                    </div>
                @elseif($row->display_name == "Father's Name")
                    <div class="col-md-12">
                        <br><h3 style="margin-left:-13px;">Family Background</h3><br>
                    </div>
                @elseif($row->display_name == "Guardian's Name")
                    <div class="col-md-12">
                        <br><h3 style="margin-left:-13px;">Guardian's Information</h3><br>
                    </div>
                @elseif($row->display_name == "Emergency Contact Name")
                    <div class="col-md-12">
                        <br><h3 style="margin-left:-13px;">Person to call in case of emergency</h3><br>
                    </div>
                @elseif($row->display_name == "Signatory Name")
                    <div class="col-md-12">
                        <br><h3 style="margin-left:-13px;">Signatory of Contract</h3><br>
                    </div>
                @endif

                @if($row->display_name == "Mother's Name")
                    <div class="col-md-12">
                        <br>
                    </div>
                @endif

                <div class="panel panel-bordered col-md-3" style="height: 100px;">

                    <!-- form start -->
                    
                    @php $rowDetails = json_decode($row->details);
                    if($rowDetails === null){
                            $rowDetails=new stdClass();
                            $rowDetails->options=new stdClass();
                    }
                    @endphp

                    <div class="panel-heading" style="border-bottom:0;">
                        <h3 class="panel-title">{{ $row->display_name }}</h3>
                    </div>

                    <div class="panel-body" style="padding-top:0;">
                        @if($row->type == "image")
                            <img class="img-responsive"
                                src="{{ filter_var($dataTypeContent->{$row->field}, FILTER_VALIDATE_URL) ? $dataTypeContent->{$row->field} : Voyager::image($dataTypeContent->{$row->field}) }}">
                        @elseif($row->type == 'multiple_images')
                            @if(json_decode($dataTypeContent->{$row->field}))
                                @foreach(json_decode($dataTypeContent->{$row->field}) as $file)
                                    <img class="img-responsive"
                                        src="{{ filter_var($file, FILTER_VALIDATE_URL) ? $file : Voyager::image($file) }}">
                                @endforeach
                            @else
                                <img class="img-responsive"
                                    src="{{ filter_var($dataTypeContent->{$row->field}, FILTER_VALIDATE_URL) ? $dataTypeContent->{$row->field} : Voyager::image($dataTypeContent->{$row->field}) }}">
                            @endif
                        @elseif($row->type == 'relationship')
                            @include('voyager::formfields.relationship', ['view' => 'read', 'options' => $rowDetails])
                        @elseif($row->type == 'select_dropdown' && property_exists($rowDetails, 'options') &&
                                !empty($rowDetails->options->{$dataTypeContent->{$row->field}})
                        )

                            <?php echo $rowDetails->options->{$dataTypeContent->{$row->field}};?>
                        @elseif($row->type == 'select_dropdown' && $dataTypeContent->{$row->field . '_page_slug'})
                            <a href="{{ $dataTypeContent->{$row->field . '_page_slug'} }}">{{ $dataTypeContent->{$row->field}  }}</a>
                        @elseif($row->type == 'select_multiple')
                            @if(property_exists($rowDetails, 'relationship'))

                                @foreach(json_decode($dataTypeContent->{$row->field}) as $item)
                                    @if($item->{$row->field . '_page_slug'})
                                    <a href="{{ $item->{$row->field . '_page_slug'} }}">{{ $item->{$row->field}  }}</a>@if(!$loop->last), @endif
                                    @else
                                    {{ $item->{$row->field}  }}
                                    @endif
                                @endforeach

                            @elseif(property_exists($rowDetails, 'options'))
                                @foreach(json_decode($dataTypeContent->{$row->field}) as $item)
                                {{ $rowDetails->options->{$item} . (!$loop->last ? ', ' : '') }}
                                @endforeach
                            @endif
                        @elseif($row->type == 'date' || $row->type == 'timestamp')
                            {{ $rowDetails && property_exists($rowDetails, 'format') ? \Carbon\Carbon::parse($dataTypeContent->{$row->field})->formatLocalized($rowDetails->format) : $dataTypeContent->{$row->field} }}
                        @elseif($row->type == 'checkbox')
                            @if($rowDetails && property_exists($rowDetails, 'on') && property_exists($rowDetails, 'off'))
                                @if($dataTypeContent->{$row->field})
                                <span class="label label-info">{{ $rowDetails->on }}</span>
                                @else
                                <span class="label label-primary">{{ $rowDetails->off }}</span>
                                @endif
                            @else
                            {{ $dataTypeContent->{$row->field} }}
                            @endif
                        @elseif($row->type == 'color')
                            <span class="badge badge-lg" style="background-color: {{ $dataTypeContent->{$row->field} }}">{{ $dataTypeContent->{$row->field} }}</span>
                        @elseif($row->type == 'coordinates')
                            @include('voyager::partials.coordinates')
                        @elseif($row->type == 'rich_text_box')
                            @include('voyager::multilingual.input-hidden-bread-read')
                            <p>{!! $dataTypeContent->{$row->field} !!}</p>
                        @elseif($row->type == 'file')
                            @if(json_decode($dataTypeContent->{$row->field}))
                                @foreach(json_decode($dataTypeContent->{$row->field}) as $file)
                                    <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($file->download_link) ?: '' }}">
                                        {{ $file->original_name ?: '' }}
                                    </a>
                                    <br/>
                                @endforeach
                            @else
                                <a href="{{ Storage::disk(config('voyager.storage.disk'))->url($row->field) ?: '' }}">
                                    {{ __('voyager::generic.download') }}
                                </a>
                            @endif
                        @else
                            @include('voyager::multilingual.input-hidden-bread-read')
                            <p>{{ $dataTypeContent->{$row->field} }}</p>
                        @endif
                    </div><!-- panel-body -->
                    {{-- @if(!$loop->last)
                        <hr style="margin:0;">
                    @endif --}}
                </div>
                @endforeach

            </div>

            <div class="col-md-12">
                <div class="col-md-12">
                    <br><h3>Billing Information</h3><br>
                </div>
                <div class="tab">
                    <button class="tablinks" onclick="openCity(event, 'Billing')" id="defaultOpen"><b>Billing History</b></button>
                    {{-- <button class="tablinks" onclick="openCity(event, 'Payment')"><b>Payment History</b></button> --}}
                </div>

                <div id="Billing" class="tabcontent">
                    <br>
                    <div class="table-responsive">
                        <table id="dataTable" class="table table-hover">
                            <thead>
                                <tr>
                                    <th><center>Bill Type</center></th>
                                    <th><center>Billing Date</center></th>
                                    <th><center>Amount Billed</center></th>
                                    <th><center>Actions</center></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($bills as $bill)
                                    <tr>
                                        <td>
                                            <center>
                                                {{ $bill->bill_name }}
                                                    {{-- @foreach($billTypes as $billType)
                                                    <td><center>{{ $billType->bill_type }}</center></td>
                                                    @endforeach --}}
                                            </center>
                                        </td>
                                        <td><center>{{ $bill->updated_at }}</center></td>
                                        <td><center>{{ $bill->amount }}</center></td>
                                        <td>
                                            <center>
                                                <div class="col-md-6">
                                                    <a href="#"><span class="voyager-file-text"></span> View Bill</a>
                                                </div>
                                                <div class="col-md-6">
                                                    <a href="{{action('Voyager\TenantsController@samplePDF', $bill->id)}}"><span class="voyager-download"></span> Download Bill</a>
                                                </div>
                                            </center>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                
                <div id="Payment" class="tabcontent">
                    <br>
                    <div class="table-responsive">
                        <table id="dataTable" class="table table-hover">
                            <thead>
                                <tr>
                                    <th><center>Payment Date</center></th>
                                    <th><center>Amount Paid</center></th>
                                    <th><center>Payment Channel</center></th>
                                    <th><center>Actions</center></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><center>hello</center></td>
                                    <td><center>it's</center></td>
                                    <td><center>me</center></td>
                                    <td>
                                        <center>
                                            <a href="#"><span class="voyager-receipt"></span> Download Receipt</a>
                                        </center>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            </div>
        </div>
    </div>
    {{-- Single delete modal --}}
    <div class="modal modal-danger fade" tabindex="-1" id="delete_modal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="{{ __('voyager::generic.close') }}"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="voyager-trash"></i> {{ __('voyager::generic.delete_question') }} {{ strtolower($dataType->display_name_singular) }}?</h4>
                </div>
                <div class="modal-footer">
                    <form action="{{ route('voyager.'.$dataType->slug.'.index') }}" id="delete_form" method="POST">
                        {{ method_field("DELETE") }}
                        {{ csrf_field() }}
                        <input type="submit" class="btn btn-danger pull-right delete-confirm"
                               value="{{ __('voyager::generic.delete_confirm') }} {{ strtolower($dataType->display_name_singular) }}">
                    </form>
                    <button type="button" class="btn btn-default pull-right" data-dismiss="modal">{{ __('voyager::generic.cancel') }}</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@stop

@section('javascript')
    @if ($isModelTranslatable)
    <script>
        $(document).ready(function () {
            $('.side-body').multilingual();
        });
    </script>
    <script src="{{ voyager_asset('js/multilingual.js') }}"></script>
    @endif
    <script>
        var deleteFormAction;
        $('.delete').on('click', function (e) {
            var form = $('#delete_form')[0];

            if (!deleteFormAction) { // Save form action initial value
                deleteFormAction = form.action;
            }

            form.action = deleteFormAction.match(/\/[0-9]+$/)
                ? deleteFormAction.replace(/([0-9]+$)/, $(this).data('id'))
                : deleteFormAction + '/' + $(this).data('id');
            console.log(form.action);

            $('#delete_modal').modal('show');
        });

        function openCity(evt, cityName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }

        // Get the element with id="defaultOpen" and click on it
        document.getElementById("defaultOpen").click();

    </script>
@stop
