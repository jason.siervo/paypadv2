@extends('voyager::master')

@push('head')
    <link rel="stylesheet" href="{{ asset('css/datatable.css') }}">
@endpush

@section('content')

    <a href="/admin/addmessage" class="pull-right btn btn-success">ADD</a>

    <table id="messagesTable">
        <thead>
            <tr>
                <th>From</th>
                <th style="max-width:300px">Message</th>
                <th>Time</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($messages as $message)
                <tr>
                    <td>{{$message->from}}</td>
                    <td>{{$message->message}}</td>
                    <td>{{$message->created_at}}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endsection

<script src="{{ asset('js/jquery.js') }}"></script>
<script src="{{ asset('js/datatable.js') }}"></script>

@push('after-scripts')
    <script>
        setTimeout(() => {
            $('#messagesTable').DataTable();
        }, 100);
    </script>
@endpush