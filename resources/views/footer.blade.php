<footer id="fh5co-footer" role="contentinfo">
	
    <div class="container">
        <div class="col-md-3 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
            <!-- <h3>DEVELOPED AND OPERATED BY</h3> -->
            <br><br>
            <p><img src="images/summithill.png" "height="200" width="220"></p>
            <!-- <p><a href="#" class="btn btn-primary btn-outline with-arrow btn-sm">Join Us <i class="icon-arrow-right"></i></a></p> -->
        </div>
        <div class="col-md-6 col-md-push-1 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
            <h3>Our Services</h3>
            <ul class="float">
                <li><a href="#">Accomodation</a></li>
                <li><a href="#">24/7 Security Service</a></li>
                <li><a href="#">RFID Card Access security</a></li>
                <li><a href="#">Lap Pool</a></li>
            </ul>
            <ul class="float">
                <li><a href="#">House Keeping</a></li>
                <li><a href="#">Gym</a></li>
                <li><a href="#">Function Rooms</a></li>
                <li><a href="#">Mobile App</a></li>
            </ul>

        </div>

        <div class="col-md-2 col-md-push-1 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
            <h3>Follow Us</h3>
            <ul class="fh5co-social">
                <li><a href="https://twitter.com/upadresidences"><i class="icon-twitter"></i></a></li>
                <li><a href="https://www.facebook.com/universitypadubelt/"><i class="icon-facebook"></i></a></li>
                <li><a href="https://plus.google.com/u/0/109961079083637448279"><i class="icon-google-plus"></i></a></li>
                <li><a href="https://www.instagram.com/upadubelt/"><i class="icon-instagram"></i></a></li>
                <li><a href="https://www.youtube.com/user/UniversityPadDorm"><i class="icon-youtube"></i></a></li>

            </ul>
        </div>
        
        
        <div class="col-md-12 fh5co-copyright text-center">
            <p>&copy; 2011-2018 UNIVERSITY PAD RESIDENCES. <br>
             All Rights Reserved. </p>
            
            <!-- 2016 Free HTML5 template. All Rights Reserved. <span>Designed with <i class="icon-heart"></i> by <a href="http://freehtml5.co/" target="_blank">FreeHTML5.co</a> Demo Images by <a href="http://unsplash.com/" target="_blank">Unsplash</a></span> -->
        </div>
        
    </div>
    </footer>
</div>
<script>

    $(function() {
        $('#form').submit(function(event) {
            var verified = grecaptcha.getResponse();
            if (verified.length === 0){
                
                alert("Please fill up the form below!");
                event.preventDefault();
            }
        });
    });

    </script>


    





<!-- jQuery -->
<script src="js/jquery.min.js"></script>
<!-- jQuery Easing -->
<script src="js/jquery.easing.1.3.js"></script>
<!-- Bootstrap -->
<script src="js/bootstrap.min.js"></script>
<!-- Waypoints -->
<script src="js/jquery.waypoints.min.js"></script>
<!-- Flexslider -->
<script src="js/jquery.flexslider-min.js"></script>
<!-- Google Map -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCefOgb1ZWqYtj7raVSmN4PL2WkTrc-KyA&sensor=false"></script>
<script src="js/google_map.js"></script>

<!-- MAIN JS -->
<script src="js/main.js"></script>

    

</body>
</html>

