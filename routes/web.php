<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// // JERRROME

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::get('/pdf', function () {
    return view('pdf');
});

Route::get('/', 'Geninquiry@index');

Route::get('about/', 'AboutController@index');
Route::get('contactus/', 'Geninquiry@create');
Route::get('services/', 'ServicesController@index');

Route::get('/downloadPDF/{id}','Voyager\TenantsController@downloadPDF');

// Route::get('/pdf', function(){
//     return view('pdf');
// });

Route::get('admin/viewmessage', 'MessageController@messages');
Route::get('admin/addmessage', 'MessageController@addmessage');
Route::post('admin/addmessage', 'MessageController@storemessage');
Route::get('admin/events2', 'EventsController@index')->name('events.index');
Route::post('admin/events2', 'EventsController@addEvent')->name('events.add');

//Routes for PDF Generation
Route::get('/PdfDemo', ['as'=>'PdfDemo','uses'=>'Voyager\TenantsController@index']);
Route::get('/sample-pdf/{id}', ['as'=>'SamplePDF','uses'=>'Voyager\TenantsController@samplePDF']);
Route::get('/save-pdf', ['as'=>'SavePDF','uses'=>'Voyager\TenantsController@savePDF']);
Route::get('/download-pdf', ['as'=>'DownloadPDF','uses'=>'Voyager\TenantsController@downloadPDF']);
Route::get('/html-to-pdf', ['as'=>'HtmlToPDF','uses'=>'Voyager\TenantsController@htmlToPDF']);

// Route::post('/sendmail', function (Illuminate\Http\Request $request,
//     \Illuminate\Mail\Mailer $mailer) {
//     $mailer
//         ->to($request->input('mail'))
//         ->send(new \App\Mail\MyMail($request->input('title')));
//     return redirect()->back();
// })->name('sendmail');