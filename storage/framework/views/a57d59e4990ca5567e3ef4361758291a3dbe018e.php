<?php echo $__env->make('header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
	
	<div class="container">
		
	</div>
	<aside id="fh5co-hero" class="js-fullheight">
		<div class="flexslider js-fullheight">
			<ul class="slides">
		   	<li style="background-image: url(images/building1.png);">
		   		<div class="overlay-gradient"></div>
		   		<div class="container">
		   			<div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
		   				<div class="slider-text-inner">
		   					<h2>The Premium student residence made affordable!</h2>
							   <!-- <p><a href="<?php echo e(url('contactus')); ?>" class="btn btn-primary btn-lg">General Inquiry</a> -->
							 
						 <p><a href="<?php echo e(url('contactus')); ?>" class="btn btn-primary btn-lg">General Inquiry</a>
							 
							  <a href="" class="btn btn-primary btn-lg">Room Inquiry</a></p>
		   				</div>
		   			</div>
		   		</div>
		   	</li>
		   	<li style="background-image: url(images/asdf.jpg);">
		   		<div class="container">
		   			<div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
		   				<div class="slider-text-inner">
		   					<h2>University Pad Residences Becomes A Home Away From Home!</h2>
							   <p><a href="<?php echo e(url('contactus')); ?>" class="btn btn-primary btn-lg">General Inquiry</a>
							  <a href="#" class="btn btn-primary btn-lg">Room Inquiry</a></p>
		   				</div>
		   			</div>
		   		</div>
		   	</li>
		   	<li style="background-image: url(images/studyarea.jpg);">
		   		<div class="container">
		   			<div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
		   				<div class="slider-text-inner">
		   					<h2>We Think Different That Others Can't</h2>
							   <p><a href="<?php echo e(url('contactus')); ?>" class="btn btn-primary btn-lg">General Inquiry</a>
							  <a href="#" class="btn btn-primary btn-lg">Room Inquiry</a></p>
		   				</div>
		   			</div>
		   		</div>
		   	</li>
		  	</ul>
	  	</div>
	</aside>

	<div id="fh5co-pricing-section">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 text-center fh5co-heading animate-box">
				<div class="price-box popular" style="background-image: url(images/stroke.png);">
				
					<h2>#YourNextHome is your Modern College Dorm</h2>
					</div>
					<p>The only choice for convenient and affordable luxury student living, University Pad Residences is just a few steps away from the country's best universities, making it your home away from home.</p>
				</div>
			</div>


			<div class="row">
				<div class="pricing">
					<div class="col-md-3 animate-box">
						<div class="price-box popular">
							<h2 class="pricing-plan"></h2>
							<div class="price"><sup class="currency"></sup>U<small> </small></div>
							<br>
							<p>niversity Pad Residences, also known as Upad is the pioneer in redefining dormitory living to a hotel like living in the Philippines.</p>
						</div>
					</div>

					<div class="col-md-3 animate-box">
						<div class="price-box popular">
							<h2 class="pricing-plan"></h2>
							<div class="price"><sup class="currency"></sup>P<small> </small></div>
							<br>
							<p>assionate to deliver a very conducive, home-like environment rooms & facilities suitable for both local and foreign students. </p>
						</div>
					</div>

					<div class="col-md-3 animate-box">
					<div class="price-box popular">
							<h2 class="pricing-plan"></h2>
							<div class="price"><sup class="currency"></sup>A<small> </small></div>
							<br>
							<p>mbitious and evolving to become the trusted partner for dormitory living. We offer fully furnished modern rooms.</p>
						</div>
					</div>

					<div class="col-md-3 animate-box">
						<div class="price-box popular">
							<h2 class="pricing-plan"></h2>
							<div class="price"><sup class="currency"></sup>D<small> </small></div>
							<br>
							<p>estined to be the first choice and the leading dormitory located near top universities in the Philippines.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	
	<div id="fh5co-work-section" class="fh5co-light-grey-section">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 text-center fh5co-heading animate-box">
					<h2>Luxurious Amenities</h2>
					<p>Upad knows how to take care of you and treat you as you want to be treated. Come and experience our secured facilities, and personalized accomodation. </p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 animate-box">
					<a href="#" class="item-grid text-center">
						<div class="image" style="background-image: url(images/lobby.jpg)"></div>
						<div class="v-align">
							<div class="v-align-middle">
								<h3 class="title">Concierge</h3>
								<h5 class="category">Friendly and Accomodating Personnel</h5>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-4 animate-box">
					<a href="#" class="item-grid text-center">
						<div class="image" style="background-image: url(images/rooms.png)"></div>
						<div class="v-align">
							<div class="v-align-middle">
								<h3 class="title">Accomodotion</h3>
								<h5 class="category">Conducive Rooms for Studying</h5>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-4 animate-box">
					<a href="#" class="item-grid text-center">
						<div class="image" style="background-image: url(images/lounge.jpg)"></div>
						<div class="v-align">
							<div class="v-align-middle">
								<h3 class="title">Student Lounge</h3>
								<h5 class="category">Comfortable area open to students for gathering & studying</h5>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-4 animate-box">
					<a href="#" class="item-grid text-center">
						<div class="image" style="background-image: url(images/chapel.jpg)"></div>
						<div class="v-align">
							<div class="v-align-middle">
								<h3 class="title">Mini Chapel</h3>
								<h5 class="category">A place for Meditation</h5>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-4 animate-box">
					<a href="#" class="item-grid text-center">
						<div class="image" style="background-image: url(images/pool.jpg)"></div>
						<div class="v-align">
							<div class="v-align-middle">
								<h3 class="title">Swimming Pool</h3>
								<h5 class="category">Well-maintaned pool</h5>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-4 animate-box">
					<a href="#" class="item-grid text-center">
						<div class="image" style="background-image: url(images/gym.jpg)"></div>
						<div class="v-align">
							<div class="v-align-middle">
								<h3 class="title">Gym</h3>
								<h5 class="category">Well-equipped & Spacious Gym</h5>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-12 text-center animate-box">
					<!-- <p><a href="#" class="btn btn-primary with-arrow">View More Projects <i class="icon-arrow-right"></i></a></p> -->
				</div>
			</div>
		</div>
	</div>
	
	<div id="fh5co-testimony-section">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 text-center fh5co-heading animate-box">
					<h2>Clients Feedback</h2>
					<!-- <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p> -->
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-offset-0 to-animate">
					<div class="wrap-testimony animate-box">
						<div class="owl-carousel-fullwidth">
							<div class="item">
								<div class="testimony-slide active text-center">
									<figure>
										<img src="https://scontent.fmnl4-2.fna.fbcdn.net/v/t1.0-1/35533268_10210031347014886_3870739576444157952_n.jpg?_nc_cat=111&oh=2916c209fd2ce6b35919ad23531b5814&oe=5C5B5C17" alt="user">
									</figure>
									<blockquote>
										<p>"Staying here helped me a lot as a reviewee since it is located only a few blocks away from my review center (and most review centers). The pool & gym helped me cool down whenever I was stressed from studying. Aside from the amenities, I definitely felt safe here because of their 24-hr security system.</p>
									</blockquote>
									<span>Pauline Andrea Roncal, via <a href="https://www.facebook.com/pauline.roncal/posts/10209927753305108:0" class="facebook">Facebook</a> | Thursday, May 31, 2018</span>
									<p>__________________________________________</p>
								</div>
							</div>
							<div class="item">
								<div class="testimony-slide active text-center">
									<figure>
										<img src="https://scontent.fmnl4-2.fna.fbcdn.net/v/t1.0-1/28168203_1672110169521241_3841962172231261111_n.jpg?_nc_cat=103&oh=f0147b148df3fe87842a7d5a7e9944e9&oe=5C5030DF" alt="user">
									</figure>
									<blockquote>
										<p>"As a reviewee. The place is very conducive for studying, since apart from the provided tables for each tenant inside the room, there was a separate study area. It was also equipped with a gym and a pool to keep your body in shape. Overall, it was a good place to stay in."</p>
									</blockquote>
									<span>Norman King Vicente, via <a href="https://www.facebook.com/Enkeyey/posts/1508885195843740:0" class="facebook">Facebook</a> | Saturday, September 9, 2017</span>
									<p>__________________________________________</p>
								</div>

							</div>
							<div class="item">
								<div class="testimony-slide active text-center">
									<figure>
										<img src="https://scontent.fmnl4-2.fna.fbcdn.net/v/t1.0-9/27858832_2239983286015619_3409877037650440468_n.jpg?_nc_cat=107&oh=2c96b966a1ce5accb8a2fbfb36f28331&oe=5C403420" alt="user">
									</figure>
									<blockquote>
										<p>"The people who lives here are great and since my school is near the building it is really a good choice. The amenities and the staff here are great! Greatly recommender for reviewies and students like me."</p>
									</blockquote>
									<span>Gabriel Gonzales, via <a href="https://www.facebook.com/dubidubigabgab/posts/1378813065465983:0" class="facebook">Facebook</a> | Monday, June 6, 2016</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="fh5co-blog-section" class="fh5co-light-grey-section">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 text-center fh5co-heading animate-box">
					<h2>Branches</h2>
					<p>University Pad Residences branches was developed by Summithill Holdings Inc. to fill-up the need of students to have a comfortable and secured safe place to stay. Visit our branches to experience a luxurious accomodation!</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6 col-sm-6 animate-box">
					<a href="#" class="item-grid">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3860.9002343461666!2d120.98732975837765!3d14.60475871052213!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397c9ff442eb94d%3A0x8030ec826f02a61d!2sUniversity+Pad+Residences!5e0!3m2!1sen!2sph!4v1539345793176"
	 width="555" height="430" frameborder="0" style="border:0" allowfullscreen></iframe>
	
						<!-- <div class="image" style="background-image: url(images/uhomemap.gif)"></div> -->
						<div class="v-align">
							<div class="v-align-middle">
								<h3 class="title">P. CAMPA BRANCH</h3>
								<!-- <h5 class="date"><span>June 23, 2016</span> | <span>4 Comments</span></h5> -->
								<p>Upad P. Campa branch is a dormitory situated near the university belt in España Manila. Upad P. Campa Dormitory branch is near the following universities like UST, FEU, PSBA, UE, CEU, NU, CEU, STI and San Sebastian.</p>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-6 col-sm-6 animate-box">
					<a href="#" class="item-grid">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3861.6300699853336!2d120.99448934963992!3d14.563134889776371!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397c979a222a937%3A0x3e3808e1e47c08e8!2sUniversity+Pad+Residences!5e0!3m2!1sen!2sph!4v1539346429415" width="555" 
					height="430" frameborder="0" style="border:0" allowfullscreen></iframe>
						<!-- <div class="image" style="background-image: url(images/uhomemap.gif)"></div> -->
						<div class="v-align">
							<div class="v-align-middle">
								<h3 class="title">P. OCAMPO BRANCH</h3>
								<!-- <h5 class="date"><span>June 22, 2016</span> | <span>10 Comments</span></h5> -->
								<p>University Pad P. Ocampo branch is a dormitory on Taft Avenue and is conveniently located in the heart of Metro Manila close to universities, malls, and hospitals. This uPad branch is near De Lasalle, St. Scholastica and DLSU-CSB universities.</p>
							</div>
						</div>
					</a>
				</div>
				<div class="col-md-12 text-center animate-box">
					<!-- <p><a href="#" class="btn btn-primary with-arrow">View More Post <i class="icon-arrow-right"></i></a></p> -->
				</div>
			</div>
		</div>
	</div>


	<!-- <div id="fh5co-pricing-section">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3 text-center fh5co-heading animate-box">
					<h2>Mission</h2>
					<p>“To provide first-class amenities, secured environment, world- class service at an affordable value.”</p>
                </div>
                <div class="col-md-6 col-md-offset-3 text-center fh5co-heading animate-box">
					<h2>Vission</h2>
					<p>“To be the premium student residence in the Philippines” </p>
				</div>
				
			</div> -->



			<!-- <div class="row">
				<div class="pricing">
					<div class="col-md-3 animate-box">
						<div class="price-box">
							<h2 class="pricing-plan">Starter</h2>
							<div class="price"><sup class="currency">$</sup>9<small>/month</small></div>
							<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
							<a href="#" class="btn btn-select-plan btn-sm">Select Plan</a>

						</div>
					</div>

					<div class="col-md-3 animate-box">
						<div class="price-box">
							<h2 class="pricing-plan">Basic</h2>
							<div class="price"><sup class="currency">$</sup>27<small>/month</small></div>
							<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
							<a href="#" class="btn btn-select-plan btn-sm">Select Plan</a>
						</div>
					</div>

					<div class="col-md-3 animate-box">
						<div class="price-box popular">
							<h2 class="pricing-plan pricing-plan-offer">Pro <span>Best Offer</span></h2>
							<div class="price"><sup class="currency">$</sup>74<small>/month</small></div>
							<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
							<a href="#" class="btn btn-select-plan btn-sm">Select Plan</a>
						</div>
					</div>

					<div class="col-md-3 animate-box">
						<div class="price-box">
							<h2 class="pricing-plan">Unlimited</h2>
							<div class="price"><sup class="currency">$</sup>140<small>/month</small></div>
							<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
							<a href="#" class="btn btn-select-plan btn-sm">Select Plan</a>
						</div>
					</div>
				</div> -->
			</div>
		</div>
	</div>

	<div class="fh5co-cta" style="background-image: url(images/upad.jpg);">
		<div class="overlay"></div>
		<div class="container">
			<div class="col-md-12 text-center animate-box">
			<p><font size="60">UPSCALE. AFFORDABLE. SECURE.</font></p>
				<!-- <p><a href="<?php echo e(url('contactus')); ?>" class="btn btn-primary btn-outline with-arrow">Inquire now! <i class="icon-arrow-right"></i></a></p> -->
			</div>
		</div>
	</div>



	

  








  
  </tbody>
</table>


<?php echo $__env->make('footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
