<!DOCTYPE html>
<html>
<head>
    <title>Monthly Billing</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    
    <link rel="stylesheet" href="<?php echo e(voyager_asset('css/bootstrap.min.css')); ?>">

    <style>
        body{
            margin: 50px;
        }
        td{
            height: 30px;
        }
        p{
            color: indigo;
        }
    </style>

</head>

<body>
    <center>
        <img src="<?php echo e(URL::asset('/images/logo1.png')); ?>" style="height: 150px;" class="css-class" alt="alt text">
    </center>

    <p>Hello</p>

    <div class="col-md-12">
        <div class="header-info">
            <div class="col-md-7">
                <!-- Tenant name and address -->
                <table>
                    <tr>
                        <td>
                            <h4><b>Name of the tenant</b></h4>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h5>Room information of tenant</h5>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-md-5">
                <!-- Summary Info of Bill -->
                <table class="table table-bordered" style="height: 50%;">
                    <tr>
                        <td>
                            <h3 style="text-align: left;"><b>Amount to Pay</b></h3> 
                            <h5 style="text-align: left;">(total amount due)</h5>
                        </td>
                        <td>
                            <h3>$500</h3>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h5><b>Bill Type</b></h5>
                            <h5>Water</h5>
                            
                        </td>
                        <td>
                            <h5><b>Tenant ID</b></h5>
                            <h5>201510971</h5>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <h5><b>Billing Period</b></h5>
                            <h5>01/01/2018 to 01/02/2018</h5>
                        </td>
                        <td>
                            <h5><b>Due Date</b></h5>
                            <h5>01/10/18</h5>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div> 
    <div class="bill-content" style="margin: 30px;">
        <h2 style="margin-bottom: 30px;">Statement of Summary</h2>

        <table class="table table-bordered">
                <tr>
                    <td><b>Charges for this month</b></td>
                </tr>
                <tr>
                    <td><b>Monthly Recurring Fee</b></td>
                </tr>
                <tr>
                    <td><b>Total</b></td>
                </tr>
        </table>

        <table class="table table-bordered">
                <tr>
                    <td><b>Previous Bill Activity</b></td>
                </tr>
                <tr>
                    <td><b>Previous Bill Amount</b></td>
                </tr>
                <tr>
                    <td><b>Remaining Balance</b></td>
                </tr>
        </table>

        <table class="table table-bordered">
                <tr>
                    <td><b>Amount to Pay</b></td>
                </tr>
        </table>
    </div>
    <div class="footer" style="margin: 30px;">

        

        <table class="table table-bordered">
            <tr>
                <td>
                    <p>Website: universitypaypad.com</p>
                    <p>Facebook Page: facebook.com/universitypadubelt/</p>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>