<?php $__env->startSection('content'); ?>
    <div class="row">
        <div class="col-md-12">
                <form action="addmessage" method="post">
                    <?php echo e(csrf_field()); ?>

                    <input type="hidden" name="from" value="<?php echo e(auth()->user()->email); ?>">
                    <label for="">Select Recipient</label>
                    <select name="to" class="form-control">
                        <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <option value="<?php echo e($user->id); ?>"><?php echo e($user->email); ?></option>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </select>
                    <label for="">Message</label>
                    <input class="form-control" type="text" name="message">
                    <input class="pull-right btn btn-success" type="Submit"></input>
                </form>
                
        </div>
    </div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('voyager::master', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>