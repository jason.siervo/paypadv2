<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tenant extends Model
{

    public function rooms(){
        return $this->belongsTo('App\Room');
    }

    // public function billMgmt(){
    //     return $this->belongsTo('App\Bill_mgmt');
    // }
}
