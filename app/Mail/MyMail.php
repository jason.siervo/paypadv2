<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MyMail extends Mailable
{
    use Queueable, SerializesModels;


    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('feutechigniters@gmail.com')
                    ->view('email.mymail');

        // return $this->from('universitypaypad.com')
        //             ->view('email.mymail')
        //             ->text('mails.demo_plain')
        //             ->with(
        //             [
        //                     'testVarOne' => '1',
        //                     'testVarTwo' => '2',
        //             ])
        //             ->attach(public_path('/images').'/demo.jpg', [
        //                     'as' => 'demo.jpg',
        //                     'mime' => 'image/jpeg',
        //             ]);
    }
}
