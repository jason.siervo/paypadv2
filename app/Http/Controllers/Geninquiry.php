<?php

namespace App\Http\Controllers;
use App\GenInquiries;
use Illuminate\Http\Request;
use Session;
use GuzzleHttp\Client;
use Illuminate\Contracts\Validation\Rule;

class Geninquiry extends Controller
{
    public function index()
    {
        return view('welcome');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('contactus');
       

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $this->validate($request, [
        //     'name' => 'required',
        //     'email' => 'required',
        //     'message' => 'required',
        //     'g-recaptcha-response' => 'required|captcha',
        //     ]);

      $geninquiry = new Geninquiry;
      $geninquiry->name = $request->input('name', false);
      $geninquiry->email = $request->input('email', false);
      $geninquiry->message= $request->input('message', false);

      $token = $request->input('g-recaptcha-response');
   
        if ($token) {
            $client = new Client();
            $response = $client->post('https://www.google.com/recaptcha/api/siteverify', [
                'form_params' => array(
                'secret' => '6Lfh83MUAAAAANYHyWAf3OlGb7tmfxSOFKluG7wU',
                'response' => $token
                )
            ]);

            $results = json_decode($response->getBody()->getContents());
           
            if ($results->success){
                $geninquiry->save();
                 return redirect()->back()->with('alert', 'Your inquiry has been sent!');

            } else{

                Session::flash('error', 'you are a robot');
                return redirect('/contactus');
            }
    }
        
     }

    // public function captchaValidate(Request $request)
    // {
    //     $request->validate([
    //         'name' => 'required',
    //         'email' => 'required|email',
    //         'message' => 'required',
    //         'captcha' => 'required|captcha'
    //     ]);
    // }
    // public function refreshCaptcha()
    // {
    //     return response()->json(['captcha'=> captcha_img()]);
    // }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
